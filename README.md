# Ailes de Cire Company
Custom Wordpress Theme
## Quick start
1. Clone the project.
2. Export `dump.sql` in your database `mysql database < dump.sql -p`.
3. Add your database settings in `wp-config.php`.
4. run server `php -S localhost:8000`.


[Lien vers le site en production](https://www.ailesdecire.com)
