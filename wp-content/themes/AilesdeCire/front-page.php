<?php get_header() ?>

<header>
<div id="home-carousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="<?= get_field('banniere_premier_slide')['url'] ?>" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="<?= get_field('banniere_second_slide')['url'] ?>" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="<?= get_field('banniere_troisieme_slide')['url'] ?>" alt="Third slide">
        </div>
    </div>
</div>
</header>
<div class="container">
    <div class="row justify-content-center">
        <a href="<?= get_permalink("40")?>" class="col-sm mb-4 home-square" id="home-identity"></a>
        <a href="/actualites" class="col-sm mb-4 home-square" id="home-actuality"></a>
        <a href="<?= get_permalink("46")?>" class="col-sm mb-4 home-square" id="home-action"></a>
    </div>
</div>

<?php get_footer() ?>