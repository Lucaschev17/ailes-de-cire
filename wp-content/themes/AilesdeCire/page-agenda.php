<?php get_header() ?>

<header>
    <img src="<?= get_field('banniere')['url'] ?>" alt="Compagny Banner" class="img-responsive">
    <h1 class="title-banner"><?= get_the_title() ?></h1>
</header>

<div class="container">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php the_content() ?>
    <?php endwhile;
    endif; ?>
</div>

<?php get_footer() ?>