<?php get_header() ?>

<header>
    <img src="<?= get_field('banniere')['url'] ?>" alt="Compagny Banner" class="img-responsive">
    <h1 class="title-banner compagnie">La compagnie</h1>
</header>

<main class="container">
    <div class="row">
        <div class="col-sm-3">
        <h2 id="presentation-nav">Présentation</h2>
        <h2 id="lequipe-nav">L'équipe</h2>
        <h2 id="nos-objectifs-nav">Nos objectifs</h2>
        </div>
        <div class="col-md">
            <div id="presentation">
            <?= get_field('presentation') ?>
            </div>
            <div id="lequipe">
            <?= get_field('lequipe') ?>
            </div>
            <div id="nos-objectifs">
            <?= get_field('nos_objectifs') ?>
            </div>
        </div>
    </div>
</main>





<?php get_footer() ?>