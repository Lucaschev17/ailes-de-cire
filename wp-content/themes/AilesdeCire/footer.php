<footer>
<nav class="navbar navbar-expand-lg navbar-dark" id="footer">
    <?php wp_nav_menu([
        'theme_location' => 'footer',
        'container' => false,
        'menu_class' => 'navbar-nav mr-auto'
    ])
    ?>
    <ul class="navbar-nav">
        <div class="row">
        <li class="nav-item mr-2">
            <a href="https://www.facebook.com/cieailesdecire/" target="_blank" class="nav-link"><i class="fab fa-facebook-square"></i></a>
        </li>
        <li class="nav-item mr-4">
            <a href="https://www.instagram.com/ailesdecire/" target="_blank" class="nav-link"><i class="fab fa-instagram"></i></a>
        </li>
        </div>
    </ul>
    <?= get_search_form() ?>
</nav>
<p class="text-center pt-2 pb-2 m-0" id="bottom-footer">© Cie Ailes de Cire | 2020</p>
</footer>
<?php wp_footer() ?>
</body>

</html>