<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head() ?>
    <link rel="icon" href="http://localhost:8000/wp-content/uploads/2020/05/logo-rond-moyen-150x150.png" type="image/icon">
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
    <script src="https://kit.fontawesome.com/01f781d474.js" crossorigin="anonymous"></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark top-navbar">
        <a class="navbar-brand" href="/">
            <img src="http://localhost:8000/wp-content/uploads/2020/05/logo-rond-moyen-150x150.png" alt="logo" class="img-fluid" width="100" height="100">
        </a>
        <button id="burger-btn" class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <?php wp_nav_menu([
                'theme_location' => 'header',
                'container' => false,
                'menu_class' => 'navbar-nav ml-auto'
            ])
            ?>
        </div>
    </nav>