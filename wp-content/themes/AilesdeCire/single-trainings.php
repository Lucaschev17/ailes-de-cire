<?php get_header() ?>

<header>
    <img src="http://localhost:8000/wp-content/uploads/2020/05/friendship.jpg" alt="Compagny Banner" class="img-responsive">
    <h1 class="title-banner">Stages & Workshops</h1>
</header>

<div class="container">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <h2><?php the_title() ?> | <?php foreach ((get_the_category()) as $category) {
                            echo $category->name . " ";
                        }    ?></h2>
            <hr>
            <?php the_content() ?>
            <p><strong>Date : </strong><?= get_field('date') ?></p>
            <p><strong>Lieu : </strong><?= get_field('lieu') ?></p>
            <p><strong>Intervenant(s) :</strong></p>
            <p><?= get_field('intervenant') ?></p>
        

            <a href="/contact" class="btn btn-outline-primary">Je souhaite participer !</a>

    <?php endwhile;
    endif; ?>
</div>

<?php get_footer() ?>