<?php get_header() ?>

<header>
    <img src="<?= get_field('banniere')['url'] ?>" alt="Compagny Banner" class="img-responsive">
    <h1 class="title-banner"><?= get_the_title() ?></h1>
</header>

<main class="container">
    <div class="row">
        <div class="col-md">
            <h2>Contactez nous !</h2>
            <p>Veuillez remplir ce formulaire pour nous contacter.</p>
        </div>
        <div class="col-md">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <?php the_content() ?>

            <?php endwhile;
            endif; ?>
        </div>
    </div>
</main>

<?php get_footer() ?>