$("#presentation-nav").css("cursor", "pointer");
$("#lequipe-nav").css({"cursor": "pointer", "color": "#4A4A4A"});
$("#nos-objectifs-nav").css({"cursor": "pointer", "color": "#4A4A4A"});
$("#presentation-nav").html("<strong>Présentation</stong>");
$("#lequipe").hide();
$("#nos-objectifs").hide();
$("#presentation-nav").click(function () {
    $("#presentation-nav").html("<strong>Présentation</stong>").css("color", "#212529");
    $("#lequipe-nav").html("L'équipe").css("color", "#4A4A4A");
    $("#nos-objectifs-nav").html("Nos objectifs").css("color", "#4A4A4A");
    $("#lequipe").hide();
    $("#nos-objectifs").hide();
    $("#presentation").show();
});
$("#lequipe-nav").click(function () {
    $("#lequipe-nav").html("<strong>L'équipe</stong>").css("color", "#212529");
    $("#presentation-nav").html("Présentation").css("color", "#4A4A4A");
    $("#nos-objectifs-nav").html("Nos objectifs").css("color", "#4A4A4A");
    $("#nos-objectifs").hide();
    $("#presentation").hide();
    $("#lequipe").show();
});
$("#nos-objectifs-nav").click(function () {
    $("#nos-objectifs-nav").html("<strong>Nos objectifs</stong>").css("color", "#212529");
    $("#lequipe-nav").html("L'équipe").css("color", "#4A4A4A");
    $("#presentation-nav").html("Présentation").css("color", "#4A4A4A");
    $("#lequipe").hide();
    $("#presentation").hide();
    $("#nos-objectifs").show();
});

$("#burger-btn").click(function () {
    if (document.getElementById('burger-btn').getAttribute('aria-expanded') == "false") {
        $(".top-navbar").attr('style', 'background-color: rgba(0, 0, 0, 0.7) !important');
    }
    else {
        $(".top-navbar").attr('style', 'background-color: rgba(0, 0, 0, 0) !important');
    }
})
