<?php get_header() ?>

<header>
    <img src="http://localhost:8000/wp-content/uploads/2020/06/trainings.jpeg" alt="" class="img-responsive">
    <h1 class="title-banner trainings">Stages & Workshops</h1>
</header>

<main class="container text-center">
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
            <div class="row">
                <div class="col-sm">
                    <p><strong><?= get_field('date') ?></strong></p>
                    <p>à <?= get_field('lieu') ?></p>
                </div>
                <div class="col-sm">
                    <h3><strong><?php the_title() ?></strong></h3>
                    <p><?php foreach ((get_the_category()) as $category) {
                            echo $category->name . " ";
                        }    ?></p>
                </div>
                <a href="<?php the_permalink() ?>" class="col-sm">En savoir plus</a>
            </div>
            <hr>
        <?php endwhile; ?>

        <?php ailesdecire_pagination() ?>
    <?php else : ?>
        <p>Aucun training pour le moment. Suivez nous sur les réseaux ou <a href="<?= get_permalink("44")?>">contactez nous</a> !</p>
    <?php endif; ?>
</main>


<?php get_footer() ?>