<?php get_header() ?>

<header>
        <img src="http://localhost:8000/wp-content/uploads/2020/06/actualites.jpeg" alt="" class="img-responsive">
        <h1 class="title-banner actualites">Actualités</h1>
</header>

<main class="container">

        <div class="mb-4">
                <a href="/actualites">Retour</a>
        </div>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <h2><?= the_title() ?></h2>
                        <?= the_content() ?>
        <?php endwhile;
        endif; ?>

</main>

<?php get_footer() ?>