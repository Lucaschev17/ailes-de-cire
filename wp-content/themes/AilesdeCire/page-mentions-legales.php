<?php get_header() ?>

<header>
    <img src="http://localhost:8000/wp-content/uploads/2020/06/actualites.jpeg" alt="" class="img-responsive">
    <h1 class="title-banner actualites"><?= the_title()?></h1>
</header>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <main class="container">
                        <?php the_content() ?>
                </main>
<?php endwhile;
endif; ?>

<?php get_footer() ?>